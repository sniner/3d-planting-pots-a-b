# Planting pots - Variants "A" and "B"

Planting pots of different sizes and two different, but very simple designs.

Each pot comes in three variants: without a hole for drainage ("Base"), with a single hole ("Hole") and with a few small holes ("Grid"). The numbers denote bottom radius, top radius and total height. The matching saucers are marked with the same numbers.

![](./img1.png)

I print with variable layer height, but 0.2 or 0.3 is ok. Two wall layers and 10% infill give a very sturdy print.

Published on [Thingiverse][1] on 17.10.2020.

License: [CC BY-SA 4.0][2]

[1]: https://www.thingiverse.com/thing:4626408
[2]: https://creativecommons.org/licenses/by-sa/4.0/

