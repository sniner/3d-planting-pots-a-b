#!/bin/bash

COLLECTION=(
    # A: normale Variante
    "plant_pot_a  40  60 110"
    "plant_pot_a  50  70 120"
    "plant_pot_a  60  80 140"
    # B: normale Variante
    "plant_pot_b  30  60 100"
    "plant_pot_b  40  70 120"
    "plant_pot_b  50  80 150"
    # B: breitere Variante
    "plant_pot_b  50  90 120"
    "plant_pot_b  60 100 130"
)

for item in "${COLLECTION[@]}"; do
    IFS=" " read -a value <<<"$item"
    python3 ./gen.py -f "${value[0]}".FCStd export \
        innerRadiusBottom="${value[1]}" \
        innerRadiusTop="${value[2]}" \
        totalHeight="${value[3]}"
done
