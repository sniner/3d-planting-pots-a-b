#!/usr/bin/env python3

import argparse
import os
import pathlib
import sys

sys.path.append('/usr/lib/freecad-python3/lib')

import FreeCAD
import Mesh


def named_cells(sheet):
    # Is there a better way to get all occupied cells?
    c = {sheet.getAlias(addr):addr for addr in
            [f"{c}{r}" for c in ("A", "B", "C") for r in range(1, 8)]}
    if None in c:
        del c[None]
    return c


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", type=pathlib.Path)
    subparsers = parser.add_subparsers(dest="command")
    params_parser = subparsers.add_parser("cells")
    export_parser = subparsers.add_parser("export")
    export_parser.add_argument("values", nargs="*",
        help="Set cell values (LABEL=VALUE)")
    return parser.parse_args()


def main():
    args = parse_args()

    doc = FreeCAD.open(str(args.file))

    params = doc.Spreadsheet
    cells = named_cells(params)

    if args.command=="cells":
        for cell in sorted(cells.keys()):
            print(f"{cell} = {params.get(cell)}")
    elif args.command=="export":
        bodies = doc.findObjects("PartDesign::Body")

        lcells = {c.lower():c for c in cells.keys()}
        variant = []
        for item in args.values:
            try:
                k,v = item.split("=", 1)
                k = k.lower()
                variant.append(v)
            except ValueError as ex:
                print(f"Error in key-value pair '{item}'': {ex}", file=sys.stderr)
                sys.exit(1)
            if k in lcells:
                params.set(lcells[k], v)
            else:
                print(f"Unknown field alias: {k}", file=sys.stderr)
                sys.exit(1)

        doc.recompute()

        variant = "-".join(variant)
        if variant:
            variant = "-" + variant

        for body in bodies:
            name = body.Label
            if name is None or name.startswith("_"):
                continue
            fname = f"{name}{variant}.stl"
            Mesh.export([body], fname)


if __name__=="__main__":
    main()

# vim: set et sw=4 ts=4 ft=python: